# Initilization of collections by extension
This is when an array, a list, or more generally a collections is initialized by explicitely listing all its elements.
For array, many languages allows that for fixed size arrays for a long time and for basic types like number and string litterals.
For dynamic arrays and complex types it can be challenging depending on the languages.
Todays, almost all the languages allows the definition of collections by extension and sometimes the syntax is intuitive and sometimes not really.
As I am used to switch from one languages to another, I very often have to google to find the exact syntax. Especially with Java :)
This page is a cheatcard to remind me the various idioms.


